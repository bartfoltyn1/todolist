from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.exceptions import ParseError
from rest_framework.exceptions import NotFound

import datetime
from django.http import Http404

from .models import Task
from .serializers import TaskSerializer, TaskPostSerializer


# Create your views here.
class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all().order_by('id')

    def get_serializer_class(self):
        if self.action == 'create' or self.action == 'partial_update':
            return TaskPostSerializer
        return TaskSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid(raise_exception=False):
            return Response('Bad request!', status=status.HTTP_400_BAD_REQUEST)
        try:
            self.perform_create(serializer)
        except ParseError as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
        print(serializer.data)
        task_id = serializer.data['id']
        headers = self.get_success_headers(serializer.data)
        return Response({"task_id": task_id}, status=status.HTTP_201_CREATED, headers=headers)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data
        data.pop('id')
        return Response(data)

    def partial_update(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            serializer = self.get_serializer(instance, data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response('', status=status.HTTP_204_NO_CONTENT)
        except Exception as e:
            print(e)
            return Response('Bad request!', status=status.HTTP_400_BAD_REQUEST)

