from rest_framework import serializers
from rest_framework.exceptions import ParseError
from .models import Task
import datetime
import pytz


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'


class TaskPostSerializer(serializers.ModelSerializer):
    done = serializers.BooleanField(required=False)
    done_date = serializers.DateTimeField(required=False)

    class Meta:
        model = Task
        fields = ('id', 'title', 'done', 'done_date')

    def create(self, validated_data):
        request = self.context['request']
        print(request.META)
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        print(ip)
        done = validated_data.pop('done', False)
        done_date = validated_data.pop('done_date', None)
        if not done and done_date is not None:
            raise ParseError("Can't be not done and have done date")
        elif done and done_date is None:
            done_date = datetime.datetime.now(pytz.utc)
        print(done_date)
        task = Task.objects.create(author_ip=ip, done=done,
                                   done_date=done_date, **validated_data)
        return task

    def update(self, instance, validated_data):
        done = validated_data.get('done', False)
        done_date = validated_data.get('done_date', None)

        should_set_date_to_null = instance.done and not done

        instance.title = validated_data.get('title', instance.title)
        instance.done = validated_data.get('done', instance.done)
        instance.done_date = validated_data.get('done_date', instance.done_date)

        if done and done_date is None:
            instance.done_date = datetime.datetime.now(pytz.utc)
        elif not done and done_date is not None:
            raise ParseError("Can't be not done and have doone date")
        elif should_set_date_to_null:
            instance.done_date = None

        instance.save()
        return instance
