from django.db import models
from django.conf import settings


# Create your models here.
class OwnedModel(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Category(OwnedModel):
    name = models.CharField(max_length=40)


class Task(OwnedModel):
    category = models.ForeignKey(Category, on_delete=models.SET_NULL,
                                 null=True, default=None)
    title = models.CharField(max_length=200)
    done = models.BooleanField(default=False)
    author_ip = models.GenericIPAddressField()
    created_date = models.DateTimeField(auto_now_add=True)
    done_date = models.DateTimeField(null=True, default=None)
