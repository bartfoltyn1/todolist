import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from "@material-ui/core/Checkbox";

const styles = theme => ({
  fragment: {
    margin: 20,
  },
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
    pardding: 20,
  },
  table: {
    maxWidrh: 700,
  },
  button: {
    margin: 10,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  spacer: {
    flexGrow: 1,
  },
});

const TaskData = (props) => {
  const { data, handleDoneClick } = props;
  console.log(data);
  return data.map(task => (
    <TableRow key={task.id}>
      <TableCell>{task.title}</TableCell>
      <TableCell align="right">{task.created_date}</TableCell>
      <TableCell align={"right"}>
        <Checkbox
          checked={task.done}
          onChange={() => {
            handleDoneClick(task.id, task.done);
          }}
        />
      </TableCell>
    </TableRow>
  )
  )
};

function TaskTable(props) {
  const { classes } = props;
  const numSelected = 0;
  const rowCount = 0;
  return (
    <div className={classes.fragment}>
      <Table className={classes.table} size="small">
        <TableHead>
          <TableRow>
            <TableCell padding="checkbox">
              <Checkbox
                indeterminate={numSelected > 0 && numSelected < rowCount}
                checked={numSelected === rowCount}
                inputProps={{ 'aria-label': 'Select all desserts' }}
              />
            </TableCell>
            <TableCell padding='none'>Title</TableCell>
            <TableCell align="right">Created date</TableCell>
            <TableCell align="right">Done</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TaskData {...props} />
        </TableBody>
      </Table>
    </div>
  );
}

TaskTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TaskTable);