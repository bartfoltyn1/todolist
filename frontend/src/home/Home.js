import React, { useEffect, useState } from 'react'
import HomeAppBar from "./HomeAppBar";
import axios from 'axios'
import TaskTable from "./TaskTable";
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import AddTaskDialog from './AddTaskDialog';
import EnhancedTable from './EnchancedTable';

const styles = theme => ({
  fragment: {
    margin: 20,
  },
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
    pardding: 30,
  },
  table: {
    minWidth: 700,
  },
  button: {
    margin: 10,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  spacer: {
    flexGrow: 1,
  },
});


const Home = (props) => {
  const { classes } = props;

  const [data, setData] = useState([]);


  const fetchTaskList = () => {
    axios.get(
      'http://127.0.0.1:8000/todolist/',
    ).then(result => setData(result.data))
  };

  const postTask = (values) => {
    axios.post(
      'http://127.0.0.1:8000/todolist/',
      values,
    ).then(result => fetchTaskList()).catch(error => console.log(error));
  };

  const handleDoneClick = (id, done) => {
    const url = `http://127.0.0.1:8000/todolist/${id}/`;
    axios.patch(url, {
      done: !done
    }).then(result => fetchTaskList());
  };

  useEffect(() => {
    fetchTaskList()
  }, []);

  return (
    <div>
      <HomeAppBar />
      <div style={{ padding: 20 }}>
        <Paper className={classes.root}>
          <div style={{ display: 'flex' }}>
            <div className={classes.spacer}></div>
            <AddTaskDialog handleAddClick={postTask} />
          </div>
          <TaskTable data={data} handleDoneClick={handleDoneClick} />
          <EnhancedTable/>
        </Paper>
      </div>
    </div>
  );
};

export default withStyles(styles)(Home);