import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { withStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import { Formik, Form } from 'formik';
import { FormikTextField } from 'formik-material-fields';


const styles = theme => ({
    table: {
        maxWidrh: 700,
    },
    button: {
        margin: 10,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
    spacer: {
        flexGrow: 1,
    },
});
const initialValues = {
    title: ''
};

function FormDialog(props) {
    const [open, setOpen] = React.useState(false);
    const { classes, handleAddClick } = props;

    function handleClickOpen() {
        setOpen(true);
    }

    function handleClose() {
        setOpen(false);
    }

    const handleSubmit = (values) => {
        setOpen(false);
        handleAddClick(values);
    }

    return (
        <div>
            <Button color="primary" variant="contained" className={classes.button} size={"small"} onClick={handleClickOpen}>
                ADD TASK
            <AddIcon className={classes.rightIcon} />
            </Button>
            <Formik
                initialValues={initialValues}
                onSubmit={handleSubmit}
            >
                {(props) => {
                    console.log(props);
                    return (
                        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                            <DialogTitle id="form-dialog-title">Add task</DialogTitle>
                            <DialogContent>
                                <Form autoComplete="off">
                                    <FormikTextField
                                        name="title"
                                        label="Title"
                                        margin="normal"
                                        fullWidth
                                    />
                                </Form>

                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleClose} color="primary">
                                    Cancel
                        </Button>
                                <Button onClick={() => handleSubmit(props.values)} color="primary">
                                    Add
                        </Button>
                            </DialogActions>
                        </Dialog>
                    )
                }}
            </Formik>
        </div>
    );
}

export default withStyles(styles)(FormDialog);